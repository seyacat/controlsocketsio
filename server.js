
_log = console.log;
global.console.log = function() {
	var traceobj = new Error("").stack.split("\n")[2].split(":");
	var file = traceobj[0].split(process.env.PWD + '/')[1];
	var line = traceobj[1];
	var new_args = [line + " >>"];
	new_args.push.apply(new_args, arguments);
	_log.apply(null, new_args);
};
function appenddata(data){
  fs.appendFile(require('os').homedir()+'/trendata.txt', data, function (err) {
    if (err) throw err;
    //console.log('Saved!');
  });
}

var fs = require('fs');

var express = require('express');
var app = express();
var server = require('http').createServer(app);
var validator = require('validator');


var io = require('socket.io')(server,{
  pingInterval: 10000,
  pingTimeout: 5000,
  });
var middleware = require('socketio-wildcard')();
io.use(middleware);

var estado = "inicio";
var maxjugadores = 4;
var deftiempo = 45;
var tiempo =0;
var gamelive = 0;
var maxconexionesxip = 2;

//WEB SERVER
app.use(express.static(__dirname + '/public'));
//redirect / to our index.html file
app.get('/', function(req, res,next) {
    res.sendFile(__dirname + '/public/index.html');
});

/*app.get('/guest/s/default/', function(req, res,next) {
    res.sendFile(__dirname + '/public/game.html');
});*/


io.on('connection', function(socket) {
	var clients = io.sockets.connected;
	var addr = socket.handshake.address;
	//console.log(addr);
	//console.log(clients);
	var conncount = 0;
	Object.keys(clients).forEach(function(key) {
			if( addr == clients[key].handshake.address ){
				conncount++
			}
			if(conncount>maxconexionesxip){
				socket.emit("duplicated");
			}
		});


  //console.log('Client connected...');
  //ADMIN COMMANDS
	socket.on('gamelive', function(data) {
		//console.log("gamelive");
		gamelive = 0;
	});
  socket.on('admin', function(data) {
    socket.join('admin');
  });

	socket.on('gameinfo', function(data) {
    //socket.join('admin');
		//console.log(data);
		var jsondata = JSON.parse(data);
		var playroom = io.sockets.adapter.rooms["play"];
	  if(playroom!=undefined){
	    //TURNOS
	    var c=0;
			var d=0;
	    for(s in playroom.sockets){
				io.sockets.connected[s].emit("gameinfo",jsondata["jugadores"][c]);
				c++;
			}
		}
		var finroom = io.sockets.adapter.rooms["fin"];
	    if(finroom!=undefined){
	    //TURNOS
	    var c=0;
			var d=0;
	    for(s in finroom.sockets){
				io.sockets.connected[s].emit("gameinfo",jsondata["jugadores"][c]);
				c++;
			}
		}

  });
  //INICIAR JUEGO
  socket.on('jugar', function(data) {
    if(estado != "inicio")return;
		resetplayroom();


    var gameroom = io.sockets.adapter.rooms["game"];
    if(gameroom!=undefined){
      var c = 0;
      gamedata = [];
      for(s in gameroom.sockets){
        io.sockets.connected[s].join("play");
        io.sockets.connected[s].leave("game");
        io.sockets.connected[s].emit("play");
        io.sockets.connected[s].player = c;
        tiempo = deftiempo;
        estado = "juego";
        gamedata.push({"jugador":c,"nickname":io.sockets.connected[s].nickname});
        c++;
        if(c>=maxjugadores)break;
      }
      if(estado=="juego"){
        sendunity({"accion":"juego","data":gamedata});
      }

  }
    //socket.join('admin');
    //estado = juego;
  });

  //CLIENT COMMAND
  socket.on('formpage', function(data) {
    socket.join('form');
  });
  socket.on('form', function(data) {
    if(data.email==undefined
      || data.nombre == undefined
      || !/[A-z\s]{3,}/.test(data.nombre)
      //|| !/[A-z\s]{3,}/.test(data.nickname)
			|| !/[A-z\s]{3,}/.test(data.email)
      //|| !validator.isEmail( data.email)

        ){
          socket.emit("fail");
          return;
        }
    appenddata(""+data.nombre+"|"+data.email+"|"+data.nickname+"\n");
    socket.nombre = data.nombre;
    socket.email = data.email;
    //socket.nickname = data.nickname;
		socket.nickname = data.nombre.split(" ")[0];
    socket.emit("ready");
    socket.join('game');
  });

    socket.on('k', function(data) {
       if(socket.player==undefined) return;
       sendunity({"accion":"k","jugador":socket.player,"direccion":data});

    });
});

//INITIAL EMIT
setTimeout(function(){ sendunity({"accion":"inicio"})},1000);

//GAME LOOP
setInterval(function(){
  var conectados = 0;
  var listos = 0;
  var jugando = 0;
	var admins = 0;
	gamelive++;
	if(gamelive>5){
		//process.exit();
	}
  var formroom = io.sockets.adapter.rooms["form"];
    //console.log(conectados);
  if(formroom!=undefined){
      conectados = formroom.length

  }
  var gameroom = io.sockets.adapter.rooms["game"];
  if(gameroom!=undefined){
      listos = gameroom.length
  }
  var playroom = io.sockets.adapter.rooms["play"];
  if(playroom!=undefined){
      jugando = playroom.length
  }




  if(estado == "juego"){
    tiempo --;
    if(tiempo <=0){
      tiempo = 0;
      sendunity({"accion":"fin"});
			emitend();
      estado = "fin";
      setTimeout(function(){
        sendunity({"accion":"inicio"})
        estado = "inicio";
      },10000);
    }
  }


  if(estado == "inicio"){
    notificaturnos();
    notificarsiguientesjugadores();
  }

	io.in('admin').emit('info', {"estado":estado,"conectados":conectados,"listos":listos,"jugando":jugando,
	"tiempo": tiempo });

},1000);

function notificaturnos(){
  var gameroom = io.sockets.adapter.rooms["game"];
    if(gameroom==undefined)return;

    //TURNOS
    var c=maxjugadores;
		var d=0;
    for(s in gameroom.sockets){
        turno = Math.floor((c-maxjugadores)/maxjugadores);
        io.sockets.connected[s].emit("info",{"turno":turno,"jugador":d%maxjugadores  });
        c++;
				d++;
    }
}

function notificarsiguientesjugadores(){
  var gameroom = io.sockets.adapter.rooms["game"];
      if(gameroom!=undefined){
      //SIGUIENTES JUGADORES
      var d=0;
      var siguientes = [];
      for(s in gameroom.sockets){
          siguientes.push({"jugador":d,"nickname":io.sockets.connected[s].nickname});
          d++;
          if(d>=maxjugadores)break;
      }
      sendunity({"accion":"siguientes","data":siguientes});
    }
    else{
      sendunity({"accion":"siguientes","data":{}} );
    }

}

function resetplayroom(){
	// //LIMPIA FINROOM
	// var finroom = io.sockets.adapter.rooms["fin"];
	// 	if(finroom!=undefined){
	// 		for(s in finroom.sockets){
	// 			io.sockets.connected[s].leave("fin");
	// 		}
	// 	}
  //MUEVE JUGADORES AL FINROOM
  var playroom = io.sockets.adapter.rooms["play"];
    if(playroom!=undefined){
      for(s in playroom.sockets){
        io.sockets.connected[s].leave("play");
        //io.sockets.connected[s].join("fin");
        //io.sockets.connected[s].emit("end");
        io.sockets.connected[s].player = undefined;
      }
    }
}

function emitend(){
  var playroom = io.sockets.adapter.rooms["play"];
    if(playroom!=undefined){
      for(s in playroom.sockets){
        //io.sockets.connected[s].leave("play");
        //io.sockets.connected[s].join("fin");
        io.sockets.connected[s].emit("end");
        //io.sockets.connected[s].player = undefined;
      }
    }
}


function sendunity(data){
  io.in('admin').emit('accion',data);
}


/*server.listen(8880, function(){
  console.log('listening on *:8880');
});*/

server.listen(80, function(){
  console.log('listening on *:80');
});


//GET IPS
'use strict';
var os = require('os');
var ifaces = os.networkInterfaces();
Object.keys(ifaces).forEach(function (ifname) {
  var alias = 0;
  ifaces[ifname].forEach(function (iface) {
    if ('IPv4' !== iface.family || iface.internal !== false) {
      // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
      return;
    }
    if (alias >= 1) {
      // this single interface has multiple ipv4 addresses
      console.log(ifname + ':' + alias, iface.address);
    } else {
      // this interface has only one ipv4 adress
      console.log(ifname, iface.address);
    }
    ++alias;
  });
});
